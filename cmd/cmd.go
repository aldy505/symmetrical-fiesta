package cmd

import (
	"database/sql"
	"symmetrical-fiesta/member"
	memberserver "symmetrical-fiesta/member/server"

	"github.com/allegro/bigcache/v3"
	"github.com/labstack/echo/v4"
	"github.com/streadway/amqp"
)

type dependency struct {
	member       *member.Dependency
	memberserver *memberserver.Dependency
}

func New(db *sql.DB, queue *amqp.Connection, memory *bigcache.BigCache) *dependency {
	memberDeps := &member.Dependency{
		DB:     db,
		Memory: memory,
	}

	return &dependency{
		member: memberDeps,
		memberserver: &memberserver.Dependency{
			Member: memberDeps,
		},
	}
}

func (d *dependency) MemberServer(r *echo.Group) {
	d.memberserver.Server(r)
}
