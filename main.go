package main

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
	"github.com/streadway/amqp"
)

func main() {
	dbURL, ok := os.LookupEnv("DATABASE_URL")
	if !ok {
		dbURL = "postgres://postgres:postgres@localhost:5432"
	}

	amqpURL, ok := os.LookupEnv("AMQP_URL")
	if !ok {
		amqpURL = "amqp://guest:guest@localhost:5672"
	}

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "3000"
	}

	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		log.Printf("error opening database: %v", err)
		return
	}
	defer func() {
		if err := db.Close(); err != nil {
			log.Printf("error closing database: %v", err)
		}
	}()

	queue, err := amqp.Dial(amqpURL)
	if err != nil {
		log.Printf("error dialing amqp: %v", err)
		return
	}
	defer func() {
		if err := queue.Close(); err != nil {
			log.Printf("error closing queue: %v", err)
		}
	}()

	memory, err := bigcache.NewBigCache(bigcache.DefaultConfig(5 * time.Minute))
	if err != nil {
		log.Printf("error creating memory cache: %v", err)
		return
	}
	defer func() {
		if err := memory.Close(); err != nil {
			log.Printf("error closing memory cache: %v", err)
		}
	}()

	app := echo.New()

	sig := make(chan os.Signal, 1)

	go func() {
		// we should recover from panic here

		if err := app.Start(":" + port); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Printf("error starting server: %v", err)
			return
		}
	}()

	signal.Notify(sig, os.Interrupt, syscall.SIGINT)
	<-sig

	shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), time.Second*10)
	defer shutdownCancel()

	if err := app.Shutdown(shutdownCtx); err != nil {
		log.Printf("error shutting down server: %v", err)
	}
}
