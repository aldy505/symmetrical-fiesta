package member

import (
	"context"
	"database/sql"
	"fmt"
)

func (d *Dependency) GetAll(ctx context.Context) ([]Member, error) {
	conn, err := d.DB.Conn(ctx)
	if err != nil {
		return []Member{}, fmt.Errorf("failed to get connection: %w", err)
	}
	defer conn.Close()

	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelReadCommitted, ReadOnly: true})
	if err != nil {
		return []Member{}, fmt.Errorf("failed to begin transaction: %w", err)
	}

	rows, err := tx.QueryContext(ctx, "SELECT id, name, email, address, gender FROM member")
	if err != nil {
		if e := tx.Rollback(); e != nil {
			return []Member{}, fmt.Errorf("failed to rollback transaction: %w", err)
		}

		return []Member{}, fmt.Errorf("failed to query member: %w", err)
	}
	defer rows.Close()

	var members []Member
	for rows.Next() {
		var member Member
		err := rows.Scan(
			&member.ID,
			&member.Name,
			&member.Email,
			&member.Address,
			&member.Gender,
		)
		if err != nil {
			return []Member{}, fmt.Errorf("failed scanning into struct: %w", err)
		}

		members = append(members, member)
	}

	err = tx.Commit()
	if err != nil {
		if e := tx.Rollback(); e != nil {
			return []Member{}, fmt.Errorf("failed to rollback transaction: %w", e)
		}

		return []Member{}, fmt.Errorf("failed to commit transaction: %w", err)
	}

	return members, nil
}
