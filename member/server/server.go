package server

import (
	"net/http"
	"symmetrical-fiesta/member"

	"github.com/labstack/echo/v4"
)

type Dependency struct {
	Member *member.Dependency
}

func (d *Dependency) Server(router *echo.Group) {
	router.GET("/", d.Get)
}

func (d *Dependency) Get(c echo.Context) error {
	members, err := d.Member.GetAll(c.Request().Context())
	if err != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{
			"error": err.Error(),
		})
	}

	return c.JSON(http.StatusOK, echo.Map{
		"members": members,
	})
}
