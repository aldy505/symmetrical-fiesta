package member

import (
	"database/sql"

	"github.com/allegro/bigcache/v3"
	"github.com/google/uuid"
)

type Gender string

const (
	GenderMale   Gender = "Male"
	GenderFemale Gender = "Female"
	GenderOther  Gender = "Other"
)

type Member struct {
	ID       uuid.UUID `json:"id"`
	Name     string    `json:"name"`
	Email    string    `json:"email"`
	Password string    `json:"-"`
	Address  string    `json:"address"`
	Gender   Gender    `json:"gender"`
}

type Dependency struct {
	DB     *sql.DB
	Memory *bigcache.BigCache
}
