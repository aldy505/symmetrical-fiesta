package adapter

import (
	"context"
	"fmt"
	"time"
)

type Provider interface {
	AcquireToken(ctx context.Context, orderId string) (Token, error)
	CreateTransaction(ctx context.Context, token Token, amount float64) (Transaction, error)
	CancelTransaction(ctx context.Context, transaction Transaction) error
}

type Token struct {
	Id         string
	ExpiryDate time.Time
}

type Transaction struct {
	Id     string
	Token  Token
	Amount float64
	Paid   bool
}

func NewToken(id string, expiryDate time.Time) (Token, error) {
	if id == "" || expiryDate.IsZero() {
		return Token{}, fmt.Errorf("invalid token")
	}

	return Token{Id: id, ExpiryDate: expiryDate}, nil
}

func NewTransaction(id string, token Token, amount float64, paid bool) (Transaction, error) {
	if id == "" || token.Id == "" || token.ExpiryDate.IsZero() || amount <= 0 {
		return Transaction{}, fmt.Errorf("invalid transaction")
	}

	return Transaction{Id: id, Token: token, Amount: amount, Paid: paid}, nil
}
