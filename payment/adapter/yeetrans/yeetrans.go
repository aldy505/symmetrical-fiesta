// yeetrans implements the yeetrans payment adapter.
package yeetrans

import (
	"context"
	"symmetrical-fiesta/payment/adapter"
)

type config struct {
	BaseURL string
}

func New(baseURL string) *config {
	return &config{BaseURL: baseURL}
}

func (c *config) AcquireToken(ctx context.Context, orderId string) (adapter.Token, error) {
	return adapter.Token{}, nil
}

func (c *config) CreateTransaction(ctx context.Context, token adapter.Token, amount float64) (adapter.Transaction, error) {
	return adapter.Transaction{}, nil
}

func (c *config) CancelTransaction(ctx context.Context, transaction adapter.Transaction) error {
	return nil
}
