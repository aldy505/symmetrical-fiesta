package wendit

import "time"

type tokenRequest struct {
	OrderId string `json:"order_id"`
}

type tokenResponse struct {
	Token     string    `json:"token"`
	ExpiredAt time.Time `json:"expired_at"`
}
