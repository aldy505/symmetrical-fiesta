// yendit provides a payment method for Yendit.
package wendit

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"symmetrical-fiesta/payment/adapter"
)

type config struct {
	APIToken string
	BaseURL  string
}

func New(baseURL, apiToken string) *config {
	return &config{
		APIToken: apiToken,
		BaseURL:  baseURL,
	}
}

func (c *config) AcquireToken(ctx context.Context, orderId string) (adapter.Token, error) {
	data, err := json.Marshal(tokenRequest{OrderId: orderId})
	if err != nil {
		return adapter.Token{}, fmt.Errorf("failed to marshal request: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.BaseURL+"/token", bytes.NewReader(data))
	if err != nil {
		return adapter.Token{}, fmt.Errorf("failed to create request: %w", err)
	}
	req.Header.Set("Authorization", "Bearer "+c.APIToken)

	client := http.DefaultClient

	resp, err := client.Do(req)
	if err != nil {
		return adapter.Token{}, fmt.Errorf("failed to send request: %w", err)
	}
	defer resp.Body.Close()

	var tokenResponse tokenResponse
	if err := json.NewDecoder(resp.Body).Decode(&tokenResponse); err != nil {
		return adapter.Token{}, fmt.Errorf("failed to decode response: %w", err)
	}

	token, err := adapter.NewToken(tokenResponse.Token, tokenResponse.ExpiredAt)
	if err != nil {
		return adapter.Token{}, fmt.Errorf("failed to create token: %w", err)
	}

	return token, nil
}

func (c *config) CreateTransaction(ctx context.Context, token adapter.Token, amount float64) (adapter.Transaction, error) {
	return adapter.Transaction{}, nil
}

func (c *config) CancelTransaction(ctx context.Context, transaction adapter.Transaction) error {
	return nil
}
